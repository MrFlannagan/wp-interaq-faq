<?php
	/*
	
	* WP Interaq FAQ Settings *
	
	*/
	
	global $wpdb;
	$table_name = $wpdb->prefix . self::PLUGIN_ABBR . 'itneraq';
	
	if( $_POST ) {
		//if question field set, add new question
		if( $_POST['wpif_q'] != '' ) {
			$wpdb->insert( 
				$table_name, 
				array( 
					'time' => current_time( 'mysql' ),
					'question' => $_POST['wpif_q'],
					'answer' => $_POST['wpif_a'],
					'approved' => 1
				) 
			);
		}
		
		//handle unanswered questions and possibly edited answers of previously answered questions
		foreach( $_POST as $key => $value ) {
			if( strpos( $key, 'remove_' ) === 0 ) {
				$wpdb->delete( $table_name, array( 'id' => intval( str_replace( 'remove_', '', $key ) ) ), array( '%d' ) );
			}
			if( strpos( $key, 'wpife_' ) === 0 && isset( $_POST['update_' . str_replace( 'wpife_', '', $key) ] ) ) {
				$curid = str_replace( 'wpife_', '', $key );
				$wpdb->update( 
					$table_name, 
					array( 
						'answer' => $value	// integer (number) 
					), 
					array( 'id' => intval( $curid ) ), 
					array( 
						'%s'
					),
					array(
						'%d'
					)
				);
				$notify = $wpdb->get_var( $wpdb->prepare(
					"
						SELECT email 
						FROM $table_name 
						WHERE id = %s
					",
					$curid
				) );
				wp_mail( $notify, 'Your FAQ Question has been updated', 'The question you submitted to the FAQ at ' . site_url() . ' has been updated.' );
			}
		}
	}
	
	$sql = "SELECT * FROM $table_name";
	$qas = $wpdb->get_results( $sql, ARRAY_A );
	
	$answered = array();
	$unanswered = array();
	
	foreach( $qas as $qa ) {
		$build_qa = array( 
			'question' => $qa['question'],
			'answer' => $qa['answer'],
			'time' => $qa['time'],
		);
		if( $qa['answer'] == '' ) {
			$unanswered[ $qa['id'] ] = $build_qa;
		} else {
			$answered[ $qa['id'] ] = $build_qa;
		}
	}
?>
<div class="wrap">
	<h2>WP Interaq FAQ</h2>
    <?php settings_errors(); ?>
	<form action='options.php' method='POST'>
	<?php
		settings_fields( 'wpif-group' );
		do_settings_sections( 'wp-interaq-faq-options' );
		submit_button(); ?>
	</table>
	</form>
	<form action='' method='POST'>
	<p><strong>Add Questions & Answers</strong></p>
	<p>Question:<br />
	<textarea name='wpif_q' cols='50' rows='5'></textarea></p>
	<p>Answer:<br />
	<textarea name='wpif_a' cols='50' rows='5'></textarea>
	</p>
	<p>
	<input type='submit' value='Save All' />
	</p>
	<h3 style='color:Red;'>Unanswered</h3>
	<?php
		foreach( $unanswered as $key => $value ) {
			?>
			<p><input type='checkbox' name='remove_<?php echo $key; ?>' /> Check for removal<br />
			<input type='checkbox' name='update_<?php echo $key; ?>' id='update_<?php echo $key; ?>' /> Add Answer<br />
			<?php echo $value['question']; ?></p>
			<p><textarea onfocus='document.getElementById("update_<?php echo $key; ?>").checked="checked";'  name='wpife_<?php echo $key; ?>' cols='50' rows='5'></textarea></p>
			<?php
		}
	?>
	<input type='submit' value='Save All' />
	<h3>Answered</h3>
	<?php
		foreach( $answered as $key => $value ) {
			?>
			<p><input type='checkbox' name='remove_<?php echo $key; ?>' /> Check for removal<br />
			<input type='checkbox' name='update_<?php echo $key; ?>' id='update_<?php echo $key; ?>' /> Update Answer<br />
			<?php echo '<strong>' . $key . '</strong> ' . $value['question']; ?></p>
			<p><textarea onfocus='document.getElementById("update_<?php echo $key; ?>").checked="checked";' name='wpife_<?php echo $key; ?>' cols='50' rows='5'><?php echo $value['answer']; ?></textarea></p>
			<?php
		}
	?>
	<input type='submit' value='Save All' />
	</form>
</div>