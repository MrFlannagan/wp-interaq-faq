=== WP Interaq FAQ ===
Contributors: Chris Flannagan, MrFlannagan
Tags: FAQ, FAQs, user submitted questions, interactive, interactive FAQ, answers, automatic additions, Simple FAQ plugin
Donate link: http://whoischris.com/donations/
Requires at least: 3.5
Tested up to: 4.3.1
Stable tag: trunk
License: GPL2
License URI: https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html

Create a FAQ page that is searchable and allows users to submit questions.
== Description ==
A simple FAQ plugin where users can submit questions.  The questions alert an admin by email.  The admin clicks a link in the email to be taken to a form where he can submit an answer and have an optional checkbox that will add the question and answer to the FAQ page.

== Installation ==
Simply upload the plugin to to your wp-content/plugins directory and activate.

1) Once activated go to settings and set the email for new questions notifications
2) Create any preloaded questions and answers on the settings page as well
3) Post shortcode [interaq] on a page or post where you would like the FAQ to reside