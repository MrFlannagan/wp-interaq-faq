<?php
/**
 * Plugin Name: WP Interaq FAQ
 * Plugin URI: http://whoischris.com
 * Description: Create a FAQ page that is searchable and allows users to submit questions.
 * Version: 1.0.0
 * Author: Chris Flannagan
 * Author URI: http://whoischris.com
 * License: GPL2
 */
 
if( ! class_exists( 'WP_Interaq_FAQ' ) )
{
    class WP_Interaq_FAQ {
		const PLUGIN_SLUG = 'wp-interaq-faq';
		const PLUGIN_ABBR = 'wpif';
		
		//Fields used for general settings
		public $field_options = array(
			'emails' => array( 'label' => 'Notify Email of New Questions: ', 'type' => 'text', 'size' => '30', 'admin' => 'text' ),
			'sorting' => array( 'label' => 'Sort Questions by ID: ', 'type' => 'text', 'size' => '30', 'admin' => 'text' )
		);
    
        /**
         * Construct the plugin object
         */
        public function __construct()
        {
            // register actions
			add_action( 'admin_init', array( &$this, 'admin_init' ) );
			add_action( 'admin_menu', array( &$this, 'add_settings' ) );
			
			// register shortcode
			add_shortcode( 'interaq', array( &$this, 'interaq_faq_sc' ) );
        } // END public function __construct
    
        /**
         * Activate the plugin
         */
        public static function activate()
        {
		    //WP File Hide uses a custom table to store file request data
			global $wpdb;

		    $table_name = $wpdb->prefix . self::PLUGIN_ABBR . 'itneraq';
	
			$charset_collate = $wpdb->get_charset_collate();
	
			$sql = "CREATE TABLE $table_name (
				  id mediumint(9) NOT NULL AUTO_INCREMENT,
				  time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
				  question text NOT NULL,
				  answer text DEFAULT '' NOT NULL,
				  email text DEFAULT '' NOT NULL,
				  approved tinyint DEFAULT '0' NOT NULL,
				  UNIQUE KEY id (id)
				) $charset_collate;";

			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			dbDelta( $sql );

			add_option( self::PLUGIN_ABBR . '_db_version', $wfph_db_version );
        } // END public static function activate
    
        /**
         * Deactivate the plugin
         */     
        public static function deactivate()
        {
            // Do nothing
        } // END public static function deactivate
		
		public function add_settings() {
			//Place a link to our settings page under the Wordpress "Settings" menu
			add_options_page( 'WP Interaq FAQ', 'Interaq FAQ', 'manage_options', self::PLUGIN_SLUG . '-options', array( $this, 'settings_page' ) );
		}
		
		public function settings_page() {
			//Include our settings page template
			include(sprintf("%s/%s_settings.php", dirname(__FILE__), self::PLUGIN_SLUG));  
		}
		
		/**
		 * hook into WP's admin_init action hook
		 */
		public function admin_init()
		{
			// Here we create our settings fields to be contained in the setting group
			register_setting( self::PLUGIN_ABBR . '-group', self::PLUGIN_ABBR . '-group' );
			add_settings_section ( self::PLUGIN_SLUG . '_main_section' , 'Primary Settings', array( $this, 'settings_callback'), self::PLUGIN_SLUG . '-options' );
			
			//loop through our defined fields and add as a setting field option
			foreach ( $this->field_options as $field => $args ) {
				add_settings_field( self::PLUGIN_ABBR . '_' . $field, $args['label'], array( $this, $args['admin'] . '_callback' ), self::PLUGIN_SLUG . '-options', self::PLUGIN_SLUG . '_main_section', array( $field ) );
			}
		} // END public static function activate
		
		public function settings_callback() {
			//do nothing
		}
		
		//create text field options
		public function text_callback( $args ) {
			$options = get_option( self::PLUGIN_ABBR . '-group' );
			$value = '';
			
			//define value of option if has been set previously
			if ( isset( $options[ self::PLUGIN_ABBR . '_' . $args[0] ] ) ) {
				$value = $options[ self::PLUGIN_ABBR . '_' . $args[0] ];
			}
			echo '<input type="text" name="' . self::PLUGIN_ABBR . '-group[' . self::PLUGIN_ABBR . '_' . $args[0] . ']" value="' . esc_attr( $value ) . '" />';
		}

		public function interaq_faq_sc( $atts ) {
			global $wpdb;
			$options = get_option( self::PLUGIN_ABBR . '-group' );
			$table_name = $wpdb->prefix . self::PLUGIN_ABBR . 'itneraq';
			$ret = '';
			if( $_POST ) {
				$wpdb->insert( 
					$table_name, 
					array( 
						'time' => current_time( 'mysql' ),
						'question' => $_POST['newquestion'],
						'email' => $_POST['your_email'],
						'approved' => 0
					) 
				);
				echo '<p>Thank you for submitting your question, you will be notified by email when answered.</p>';
				wp_mail( $options[ self::PLUGIN_ABBR . '_emails' ], 'A new FAQ question has been submitted', 'You may answer the question at: ' . site_url() . '/wp-admin/options-general.php?page=wp-interaq-faq-options' );
			}
			$ret .= '<a href="#none" onclick="document.getElementById(\'sbfrm\').style.display=\'block\';" >Ask a question</a><div id="sbfrm" style="display:none;"><form action="" method="POST">Your Email: <input style="margin-bottom: 10px;" type="text" name="your_email"><br /><textarea name="newquestion" cols="50" rows="5" style="margin-bottom: 10px;"></textarea><br /><input type="submit" value="Submit Question" /></form></div>';
			
			$sql = "SELECT * FROM $table_name";
			$qas = $wpdb->get_results( $sql, ARRAY_A );
			
			$answered = array();
			$unanswered = array();
			$sorting = array();
			$tokeysort = explode( ',', $options[ self::PLUGIN_ABBR . '_sorting' ] );
			
			foreach( $qas as $qa ) {
				if( $qa['answer'] != '' ) {
					$sorting[$qa['id']] = '<div style="margin-top:20px;margin-bottom:20px;padding:20px;border:solid 1px #ccc;"><h3>' . $qa['question'] . '</h3><p>' . $qa['answer'] . '</p></div>';
					//$ret .= '<!--' . $qa['email'] . '//--><div style="margin-top:20px;margin-bottom:20px;padding:20px;border:solid 1px #ccc;"><h3>' . $qa['question'] . '</h3><p>' . $qa['answer'] . '</p></div>';
				}
			}
			
			foreach( $tokeysort as $qa ) {
				$ret .= $sorting[$qa];
				//$ret .= '<!--' . $qas[intval($qa)]['email'] . '//--><div style="margin-top:20px;margin-bottom:20px;padding:20px;border:solid 1px #ccc;"><h3>' . $qas[intval($qa)]['question'] . '</h3><p>' . $qas[intval($qa)]['answer'] . '</p></div>';
			}
			
			return $ret;
		}
	}
} // END if(!class_exists('WP_Interaq_FAQ'));

// Add a link to the settings page onto the plugin page
if( class_exists( 'WP_Interaq_FAQ' ) )
{			
	// Installation and uninstallation hooks
	register_activation_hook( __FILE__, array( 'WP_Interaq_FAQ', 'activate' ) );
	register_deactivation_hook( __FILE__, array( 'WP_Interaq_FAQ', 'deactivate' ) );

	// instantiate the plugin class
	$WP_Interaq_FAQ = new WP_Interaq_FAQ();
}